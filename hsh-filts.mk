src/System/Log/Log.hsh: src/System/Log.hsh
	cp $< $@

src/System/Log/Formatter.hsh.filt: src/System/Log/Formatter.hsh
	cat $< | \
	sed -e 's/ IO / IOx /g' | \
	cat > $@

src/System/Log/Handler/Growl.hsh.filt: src/System/Log/Handler/Growl.hsh
	echo data GrowlHandler > $@
	cat $< | \
	cat >> $@

src/System/Log/Handler/Simple.hsh.filt: src/System/Log/Handler/Simple.hsh
	cat $< | \
	grep -v "^data GenericHandler " | \
	cat > $@
	echo "data GenericHandler a :: * -> *" >> $@
	echo "priority :: GenericHandler a -> Priority" >> $@
	echo "formatter :: GenericHandler a -> LogFormatter (GenericHandler a)" >> $@
	echo "privData :: GenericHandler a -> a" >> $@
	echo "writeFunc :: GenericHandler a -> a -> String -> IO ()" >> $@
	echo "closeFunc :: GenericHandler a -> a -> IO ()" >> $@
